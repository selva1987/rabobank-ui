import { Component, OnInit, ViewChild } from '@angular/core';
import { IssueTracker } from '../models/IssueTracker';
import {
  MatTableDataSource,
  MatSort,
  MatPaginator,
  MatSnackBar
} from '@angular/material';

@Component({
  selector: 'app-issue-tracker',
  templateUrl: './issue-tracker.component.html',
  styleUrls: ['./issue-tracker.component.scss']
})
export class IssueTrackerComponent implements OnInit {
  public errorMessage: string;
  public filterValue: number;
  public issueTrackerList: IssueTracker[] = [];
  public tableDateList: IssueTracker[] = [];
  public displayedColumns: string[] = [
    'firstName',
    'surName',
    'issueCount',
    'dateofBirth'
  ];
  dataSource = new MatTableDataSource(this.tableDateList);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public snackBar: MatSnackBar) {}

  ngOnInit() {}

  converCsvFile(files: FileList) {
    const file: File = files.item(0);
    if (files && files.length > 0) {
      if (file.type === 'application/vnd.ms-excel') {
        const reader: FileReader = new FileReader();
        reader.readAsText(file);
        reader.onload = e => {
          const csvs: string = reader.result as string;
          const allTextLines = csvs.split(/\r|\n|\r/);
          const headers = allTextLines[0].split(',');

          for (let i = 1; i < allTextLines.length; i++) {
            const data = allTextLines[i].split(',');
            if (data.length === headers.length) {
              const tarr = [];
              for (let j = 0; j < headers.length; j++) {
                tarr.push(data[j].replace('"', ''));
              }
              this.filterValue = null;
              const issueTracker = new IssueTracker();
              issueTracker.firstName = tarr[0].replace('"', '');
              issueTracker.surName = tarr[1].replace('"', '');
              issueTracker.issueCount = tarr[2].replace('"', '');
              issueTracker.dateofBirth = tarr[3].replace('"', '');
              this.issueTrackerList.push(issueTracker);
            }
          }
          if (this.issueTrackerList.length > 0) {
            this.tableDateList = this.issueTrackerList;
            this.dataSource = new MatTableDataSource(this.tableDateList);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.dataSource.filterPredicate = function(
              data,
              filter: string
            ): boolean {
              return data.issueCount <= filter;
            };
          }
        };
      } else {
        this.filterValue = null;
        this.tableDateList = [];
        this.issueTrackerList = [];
        this.dataSource = new MatTableDataSource(this.tableDateList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.dataSource.filterPredicate = function(
          data,
          filter: string
        ): boolean {
          return data.issueCount <= filter;
        };
        this.snackBar.open(
          'Please upload only csv formatted file.',
          'Got it!',
          {
            duration: 3000
          }
        );
      }
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
